package importantFeatures;

import java.util.Arrays;
import java.util.List;



public class PersonMain {

	public static void main(String[] args) {
      
List<Person>persons = Arrays.asList(
        new Person("Daksh",9),
        new Person("Milan",25),
        new Person("Sharath",29));
Person result = persons.stream().filter(n->"Daksh".equals(n.getName()))
.findAny().orElse(null);
System.out.println(result);    
Person result2 = persons.stream().filter((p)->"Milan".equals(p.getName()) &&  25== p.getAge()).findAny().orElse(null);
System.out.println(result2);
}



}
