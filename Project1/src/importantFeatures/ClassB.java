package importantFeatures;

@FunctionalInterface
interface MyInterface2{
	   void method1();    // Functional interface contains SINGLE ABSTRACT METHOD
                          // so it can also  called SAM interface 
        static void method2() { // static method 
        	System.out.println("This is static method ");
        }
}
public class ClassB {

	public static void main(String[] args) {
		MyInterface2 my=()->
            	System.out.println("This is lamda expression demo");
            my.method1();  // calling functional interface method 
	}

}
