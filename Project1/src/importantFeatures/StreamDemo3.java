package importantFeatures;

import java.util.Arrays;
import java.util.List;



public class StreamDemo3 {



   public static void main(String[] args) {
        List<Integer> nums=Arrays.asList(5,7,10,11,13,3,2);
        nums.stream()
        .sorted()
        .forEach(n->System.out.println(n));
        System.out.println("____________");
        
        
        nums.stream()
        .filter(n->n%2==1)
        .forEach(n->System.out.println(n)); //5 13 3
        System.out.println("____________");
        
        nums.stream()
        .sorted()
        .filter(n->n%2==1)
        .forEach(n->System.out.println(n));  //3 5 13
        
        nums.stream()
        .map(n->n*2)
        .forEach(n->System.out.println(n));
        
    }



}