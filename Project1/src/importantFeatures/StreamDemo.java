package importantFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StreamDemo {
	    public static void main(String[] args)
	    {
	List<String> names = new ArrayList<>();
	names.add("Yogesh");
	names.add("Ranya");
	names.add("Ranjitha");
	names.add("Divya");
	names.add("Arya");
	names.add("Nishanka");



	//streams and lambda
	long count = names.stream().filter(str->str.length()<5).count();
	System.out.println(count+" names with less than 5 characters");



	// creating a stream using Stream.of
	Stream<String> names2 = Stream.of("Prabhu","Yashaswini");
	names2.forEach(System.out::println);



	   
	}
	}