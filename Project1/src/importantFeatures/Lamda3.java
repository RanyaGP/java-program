package importantFeatures;

@FunctionalInterface
interface Cab2{
	// abstract method 
	public double method1(String start, String destination, int km, double rupees);
}

public class Lamda3 {

	public static void main(String[] args) {
		   Cab2 abc= (start,destination,km,rupees)->{
		 System.out.println("Cab booked from "+start+ " to " +destination+ " and Total cost of trip is ");
		 return (km*rupees);
		   }; 
		System.out.println(abc.method1("Delhi","Mumbai", 1500, 200)); 
			}
}
