package importantFeatures;

interface DisplayInterface{
	void display();
}
public class MethodReference3 {
public void sayHello() {
	System.out.println("This is Method Reference..");
}
	public static void main(String[] args) {
	MethodReference3 obj = new MethodReference3();
	DisplayInterface dis = obj::sayHello;
	dis.display();
	}

}
