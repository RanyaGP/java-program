package importantFeatures;

interface argument
{
    public void greet(String s1, String s2);
}

public class LambdaExample3
{
    public static void main(String[] args)
    {
        argument mylambda= (s1,s2) -> {System.out.println("Hello "+s1+" "+s2);};
        
        mylambda.greet("This is","statement using lambda expression.");
    }
}