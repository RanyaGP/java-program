package importantFeatures;

@FunctionalInterface  
interface feature{  
    String say(String message);  
}  
  
public class LambdaExample2{  
    public static void main(String[] args) {  
      
        // You can pass multiple statements in lambda expression  
        feature person = (message)-> {  
            String str1 = "This is multiple statement ";  
            String str2 = str1 + message;   
            return str2;  
        };  
            System.out.println(person.say("using lambda expression."));  
    }  
}  