package importantFeatures;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDemo2 {

	public static void main(String[] args) {
		List<String> names = Arrays.asList("San","Ranya","Lavanya");
		List<String>othernames=names.stream().filter(n->!"San".equals(n))
				.collect(Collectors.toList());
		othernames.forEach(System.out::println);
	}

}
