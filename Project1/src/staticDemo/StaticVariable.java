package staticDemo;

public class StaticVariable {
	
	 static int x=455;  // static variable 
	        int y=546546;  // instance variable 

	public static void main(String[] args) {
		 System.out.println(StaticVariable.x);  // no need object to call static variable

		 StaticVariable st = new StaticVariable ();

		 System.out.println(st.y);  // need object to call instance variable

	}

}
