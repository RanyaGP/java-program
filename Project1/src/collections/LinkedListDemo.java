package collections; 

import java.util.LinkedList; 

public class LinkedListDemo { 

 public static void main(String[] args) { 
            LinkedList<String>sports = new LinkedList<>(); 
            sports.add("Tennis"); 
            sports.add("Volley ball"); 

            System.out.println(sports); 
            sports.add(0,"kho-kho"); 
            System.out.println(sports); 
            sports.addLast("cricket"); 
            System.out.println(sports); 

               String s = sports.get(3); 
               System.out.println(s); 
               sports.addFirst("somegame"); 
               System.out.println(sports); 

 } 

}
