package abstraction;

public  abstract class Animal {
	abstract void methodA();
	void regularMethod() {
		System.out.println("This is my regular method...");
	}

}
