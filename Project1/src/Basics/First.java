package Basics;

public class First {
	//method creation
	public void mymethod() {
		System.out.println("My first method logic...");
	}
	
	public void method2() {
		System.out.println("My second method logic...");
	}
	public static void main(String[] args) {
		System.out.println("This is my first JAVA program");
		First abc = new First();
		abc.method2();
		abc.mymethod();
	}

}
