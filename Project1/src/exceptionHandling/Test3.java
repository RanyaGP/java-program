package exceptionHandling;

public class Test3 {
static void voter(int age) {
	if(age<21) {
		throw new ArithmeticException("You are a minor.You are not eligible to vote");
	else {
		System.out.println("You can vote");
	}
}
public static void main(String[] args) {
		voter(17);
	}

}
