package filehandling;

import java.io.File;

public class GetFileInformation {

	public static void main(String[] args) {
		File obj=new File("C:\\Users\\241354\\git\\assignment\\Assign\\myfile.txt");
		
		System.out.println(obj.exists());
		if(obj.exists()) {
			System.out.println("Filename:"+obj.getName());
			System.out.println("My file addresss:"+obj.getAbsolutePath());
			System.out.println("Check writable or not"+obj.canWrite());
			System.out.println("Check readable or not:"+obj.canRead());
			System.out.println("File size in bytes:"+obj.length());
		}
	}

}
