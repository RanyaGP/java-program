package operators;

public class ArithmeticOperator {

		  // +  -  * /  %

		public static void main(String[] args) {
//	         int a=10;  // declared  and assigned value to variable 'a'
//	          int b=20;
			
		      int a=10, b=20;
	          
			System.out.println("addition operator :" +(a+b));
			System.out.println("subtraction operator :" +(b-a));
			System.out.println("multiplication operator :" +(a*b));
			System.out.println("division operator :" +(b/a));
			System.out.println("modulus operator :" +(b%a));
	         
		}

	}
